package suitebot3;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import suitebot3.BotRequestHandler;
import suitebot3.game.GameState;
import suitebot3.game.GameStateCreator;
import suitebot3.game.Moves;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.ai.BotAi;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BotRequestHandlerTest
{
    private BotRequestHandler requestHandler;

    @Before
    public void setUp() throws Exception
    {
        requestHandler = new BotRequestHandler(gameSetup -> new DummyBotAi());
    }

    @Test
    public void onSetupMessage_initializeShouldBeCalled() throws Exception
    {
        String aiMove = requestHandler.processRequest(createGameStateString(1));
        assertThat(aiMove, is("FIRSTMOVE"));
    }

    @Test
    public void onMovesMessage_makeMoveShouldBeCalled() throws Exception
    {
        requestHandler.processRequest(createGameStateString(1));
        String aiMove = requestHandler.processRequest(createGameStateString(2));
        assertThat(aiMove, is("NEXTMOVE"));
    }

    @Test
    public void onInvalidRequest_noAiMethodsShouldBeCalled() throws Exception
    {
        requestHandler.setSuppressErrorLogging(true);
        requestHandler.processRequest("invalid request");
    }

    private String createGameStateString(int currentRound) {
        GameStateDTO dto = GameStateCreator.fromString("").toDto();
        dto.currentRound = currentRound;
        dto.remainingRounds = 150 - currentRound;

        return new Gson().toJson(dto);
    }

    class DummyBotAi implements BotAi
    {
        @Override
        public Moves makeMoves(GameState gameState)
        {
            final String action;
            if (gameState.getCurrentRound() == 1)
                action = "FIRSTMOVE";
            else if (gameState.getCurrentRound() == 2)
                action = "NEXTMOVE";
            else
                throw new RuntimeException("unexpected round");

            return new Moves() {
                @Override
                public String serialize()
                {
                    return action;
                }
            };
        }

    }
}