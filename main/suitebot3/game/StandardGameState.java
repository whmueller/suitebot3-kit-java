package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

public class StandardGameState implements GameState
{
	protected int roundsRemaining;
	protected int currentRound;

	public StandardGameState(int currentRound, int roundsRemaining)
	{
		this.currentRound = currentRound;
		this.roundsRemaining = roundsRemaining;
	}

	@Override
	public int getCurrentRound()
	{
		return currentRound;
	}

	@Override
	public int getRoundsRemaining()
	{
		return roundsRemaining;
	}

	@Override
	public GameStateDTO toDto()
	{
		return new GameStateDTO();
	}
}
